from django.db import models


# Create your models here.
class TodoList(models.Model):
    name = models.CharField(max_length=100)
    created_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.name


class TodoItem(models.Model):
    task = models.CharField(max_length=100)
    # make constraint optional
    due_date = models.DateTimeField(
        null=True,
        blank=True,
    )
    is_completed = models.BooleanField(default=False)
    # "list" is ForeignKey field name
    # • establish relationship btwn TodoItem and TodoList models
    # • create column "list_id" in TodoItem database
    # • match "id" column in TodoList database
    list = models.ForeignKey(
        # "item" accesses TodoItem fields related to TodoList
        # ex. TodoList instance called "my_list"
        # my_list.items.all() accesses all TodoItem instances
        # related to TodoList instance
        "TodoList",
        related_name="items",
        on_delete=models.CASCADE,
    )

    # def __str__(self):
    #     return self.name
