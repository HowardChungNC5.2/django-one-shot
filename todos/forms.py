# import forms module from django
from django import forms

# import ModelForm class from forms module (above)
from django.forms import ModelForm

# import TodoList model class from todos.models module
from todos.models import TodoList, TodoItem


# Form for TodoList model instance that will
# show the name field in the form and
# handle the form submission to create a new TodoList
class TodoListForm(ModelForm):
    class Meta:
        # associate the form with the TodoList model
        model = TodoList
        # specify the fields in this form
        fields = ("name",)
        # specify labels for corresponding fields {field: label}
        labels = {"name": "Name:"}


class TodoItemForm(ModelForm):
    class Meta:
        model = TodoItem
        fields = (
            "task",
            "due_date",
            "is_completed",
            "list",
        )
