# import render, get_object_or_404, redirect functions from django.shortcuts
# import TodoList model from todos.models
# import TodoList Form from todos.form
from django.shortcuts import render, get_object_or_404, redirect
from todos.models import TodoList, TodoItem
from todos.forms import TodoListForm, TodoItemForm


# list view
def todo_list_list(request):
    todo_list = TodoList.objects.all()
    # context key is for template rendering
    context = {"todo_list": todo_list}
    # return render(request, template file path, context)
    return render(request, "todos/list.html", context)


# detail view
def todo_list_detail(request, id):
    list_detail = get_object_or_404(TodoList, id=id)
    # context key is for template rendering
    context = {"list_detail": list_detail}
    # return render(request, template file path, context)
    return render(request, "todos/detail.html", context)


# create forms.py for ToDoListForm model form class
# fields = ('name',)
# create new TodoList
# redirect to detail page for to-do list
def todo_list_create(request):
    # import get_object_or_404 function from django.shortcuts module
    # check if request is HTTP POST or HTTP GET
    if request.method == "POST":
        # COMMENT call on Form not model instance
        # create TodoListForm in forms.py
        form = TodoListForm(request.POST)
        # if form entries are valid
        if form.is_valid():
            # save values to the database
            list_create = form.save()
            # import redirect from django.shortcuts module
            # return redirect(param1, param2) statement to detail view (line 18)
            # param1: by views.py function name
            # param2: and specify current form .id (unique identifier)
            return redirect("todo_list_detail", id=list_create.id)
    # otherwise, since form is HTTP GET
    else:
        # create empty TodoListForm model form class
        form = TodoListForm()
    # context key is for template rendering
    # context value is empty form
    context = {
        "form": form,
    }
    # import render function from django.shortcuts module
    # return render(request, template file path, context)
    return render(request, "todos/create.html", context)


# FEATURE 10: CREATE UPDATE VIEW FUNCTION
# Create an todo_list_update view function for TodoList model instance
# get object to be edited using get_object_or_404 function
# that will show the name field in the form submission
# to change an existing TodoList instance
# redirect to detail page for current TodoList instance
# takes two params (HTTP request from client, specific todo list to be edited)
def todo_list_update(request, id):
    # get TodoList specific instance that matches given id value
    # using get_object_or_404(model, id=id)
    # retrieve TodoList object OR return 404 page
    list = get_object_or_404(TodoList, id=id)
    # if request is HTTP POST
    # user submitted a form
    if request.method == "POST":
        # create instance of TodoList model form
        # create TodoListForm in forms.py
        # bind TodoListForm data from given request to instantiated list object
        form = TodoListForm(request.POST, instance=list)
        # check submitted form data is valid
        if form.is_valid():
            # save updated form data to data base
            form.save()
            # redirect to todo_list_detail view, passing id param to specific list)
            return redirect("todo_list_detail", id=id)
    # otherwise, the request method is GET
    else:
        # create empty form passing instance of current list
        form = TodoListForm(instance=list)
        # create context dict to pass empty form of current list
        context = {
            "form": form,
        }
        # render template with provided context
        # return render(request, template file path, context)
        return render(request, "todos/edit.html", context)


# FEATURE 11: create todo_list_delete function
# func takes in (HTTP request object, id parameter from URL)
def todo_list_delete(request, id):
    # retrieve TodoList object specified by id parameter
    list = get_object_or_404(TodoList, id=id)
    # if client side HTTP request is POST (i.e. form was submitted)
    if request.method == "POST":
        # delete retrieved TodoList object
        list.delete()
        # redirect to list view by views.py function name
        return redirect("todo_list_list")
    # otherwise, the request method is GET

    context = {"delete_todo": list}
    # if request is GET render delete.html template
    return render(request, "todos/delete.html", context)


# FEATURE 12:
# create todo_item_create view func
# for TodoItemForm model showing task field
# and handle form submission to create a new TodoItem
# return redirect(request, html template file path, context)


def todo_item_create(request):
    if request.method == "POST":
        form = TodoItemForm(request.POST)
        if form.is_valid():
            item = form.save()
            # redirect to urls.py name pattern for detail view function
            # passing id property by TodoItem ForeignKey "list" relationship
            return redirect("todo_list_detail", id=item.list.id)
    else:
        form = TodoItemForm()
    context = {
        "form": form,
    }
    return render(request, "todos/item_create.html", context)


# FEATURE 13: update item view
# takes in (HTTP request object, id of TodoItem)
def todo_item_update(request, id=id):
    # grab item from TodoItem model, with given id, from database
    # using .objects.get() method
    item = TodoItem.objects.get(id=id)
    # if given request is HTTP POST
    # it means user submitted a post to update the TodoItem model
    if request.method == "POST":
        # create a new form instance of TodoItemForm
        # with data from the HTTP POST request
        # and associate this data with the retrieved item from TodoItem
        form = TodoItemForm(request.POST, instance=item)
        # if updated form is valid
        if form.is_valid():
            # save form data to the retrieved item in TodoItem
            item = form.save()
            # then redirect user to detail view function
            # passing id parameter set to (.list) ForeignKey of TodoItem
            # and, calling the (.id) field/column in the TodoList database
            return redirect("todo_list_detail", id=item.list.id)
    # otherwise, request is HTTP GET
    else:
        # create new form instance of TodoItemForm
        # with existing item data from TodoItem
        form = TodoItemForm(instance=item)
    # create context dictionary with form instance
    context = {
        "form": form,
    }
    # render update.html template with context data
    return render(request, "todos/update.html", context)
