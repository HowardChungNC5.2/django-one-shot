# One Shot Planning

These are some markdown formatting you can use to plan. You can preview your markdown in VS Code with the command "markdown preview", commonly `Command + Shift + V` (`Ctrl + Shift + V` on Windows) while you have this file open

Edit this and make it your own. Alternatively, link your notion here:

## My Notion / weblink

https://notion...

## Steps
* [X] Feature 1
* [X] Feature 2
* [X] Feature 3
* [X] Feature 4
* [X] Feature 5
* [X] Feature 6
* [X] Feature 7
* [X] Feature 8
* [X] Feature 9
* [X] Feature 10
* [X] Feature 11
* [X] Feature 12
* [X] Feature 13

### Step 1

* [ ] First substep
* [ ] Second substep

### Step 2

* [ ] First substep
* [ ] Second substep

## Questions

How do I access the model instances of the reverse relationship of a many to many relationship?

## Resources

* https://ccbv.co.uk/
